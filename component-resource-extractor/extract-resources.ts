import { concat } from "https://deno.land/std@0.125.0/bytes/mod.ts"
import   init, { HTMLRewriter } from "https://deno.land/x/lol_html/mod.js"
import   decodeWasm             from "https://deno.land/x/lol_html/wasm.js"
// https://github.com/ije/deno-lol-html

import type { Data }        from "lume/core.ts"
import { parseFrontMatter } from "lume/core/loaders/yaml.ts"
import { merge }            from "lume/core/utils.ts"
// needs the import_map.json

async function extract(html:string, tags:string[]) {
	// Tags to extract
	let tags_: Record<string, any> =
		Object.fromEntries(tags.map( t => [ t,[] ] ))
	// HTML document
	let doc  : any = []
	
	const enc = new TextEncoder()
	const dec = new TextDecoder()
	await init(decodeWasm());
	const rewriter = new HTMLRewriter("utf8",(chunk:any) => {
	  doc.push(chunk);
	});
	
	Object.keys(tags_).map(k => {
		rewriter.on(k, {
			element(el:any) { el.remove() },
			text(el:any) { tags_[k].push(
				el.text.replace(/^\t/gm,'').trim()
			)}
		})
	})
	
	rewriter.write(enc.encode( html ))
	rewriter.end(); rewriter.free();
	doc = dec.decode(concat(...doc)).trim()
	
	Object.keys(tags_).map(k=>{
		tags_[k] = tags_[k].join('')
	})
	
	return Object.assign(tags_, {"content": doc});
}

function renameKey(ob:any, ok:string, nk:string) {
	ob[nk] = ob[ok]; delete ob[ok]
}
function cleanFields(obj:any) {
	Object.keys(obj)
		.forEach(key => obj[key] === undefined && delete obj[key])
}

// Loader
export async function loadHtml(path:string): Promise<Data> {
	const content = await Deno.readTextFile(path);
	// parse front matter as normally for text sources
	const first = parseFrontMatter(content,path)
	
	var extracted =
		await extract(first.content as string, ["style","script"])
	renameKey(extracted, 'style', 'css')
	renameKey(extracted, 'script', 'js')
	cleanFields(extracted)
	const second = merge(first, extracted)
	
	return second
}
