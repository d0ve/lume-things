# Things to do with [Lume](https://lume.land/)

## Component resource extractor

Plugin for extracting component resources (css and js) from the
`<style>` and `<script>` tags (so you don't need to write them in the front matter)

### Usage
```ts
// _config.ts
import res_extract from "./plugins/extract-with-nunjucks.ts";

/*...*/

site.use(res_extract({
		// config, see the Lume Nunjuck configuration
	}))
```

```html
<!-- src/_components/component.html.njk -->
<script>
// your component script
</script>

// your html nodes

<style>
// your component style
</style>
```
